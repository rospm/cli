# Copyright (C) Austin Deric
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

r"""

Usage: rospm <command>

where <command> is one of:
    config, create, init, install, list, login, logout, ls, publish, 
    run, run-script, whoami

rospm <command> -h  quick help on <command>
rospm -l            display full usage info
rospm help <term>   search for help on <term>
rospm help rospm      involved overview

Specify configs in the ini-formatted file:
    /home/<>/.rospmrc
or on the command line via: rospm <command> --key value
Config info can be viewed via: rospm help config

"""

import fire


class rospm(object):
  """Provides a simple interface to the ROS package manager
  """


  def config():
    return 

  def create():
    return 

  def init():
    return 

  def install():
    return 

  def list():
    return 

  def ln():
    return 

  def login():
    return 

  def logout():
    return 

  def ls():
    return 

  def publish():
    return 

  def run():
    return 
    
  def run-script():
    return 

  def whoami():
    return 

def main():
  fire.Fire(rospm, name='rospm')

if __name__ == '__main__':
  main()

